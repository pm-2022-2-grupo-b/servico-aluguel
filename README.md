## Sonar Cloud - Microsserviço Aluguel
[![SonarCloud](https://sonarcloud.io/images/project_badges/sonarcloud-white.svg)](https://sonarcloud.io/summary/new_code?id=pm-2022-2-grupo-b_servico-aluguel)

[![Bugs](https://sonarcloud.io/api/project_badges/measure?project=pm-2022-2-grupo-b_servico-aluguel&metric=bugs)](https://sonarcloud.io/summary/new_code?id=pm-2022-2-grupo-b_servico-aluguel)
[![Vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=pm-2022-2-grupo-b_servico-aluguel&metric=vulnerabilities)](https://sonarcloud.io/summary/new_code?id=pm-2022-2-grupo-b_servico-aluguel)
[![Code Smells](https://sonarcloud.io/api/project_badges/measure?project=pm-2022-2-grupo-b_servico-aluguel&metric=code_smells)](https://sonarcloud.io/summary/new_code?id=pm-2022-2-grupo-b_servico-aluguel)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=pm-2022-2-grupo-b_servico-aluguel&metric=coverage)](https://sonarcloud.io/summary/new_code?id=pm-2022-2-grupo-b_servico-aluguel)
[![Duplicated Lines (%)](https://sonarcloud.io/api/project_badges/measure?project=pm-2022-2-grupo-b_servico-aluguel&metric=duplicated_lines_density)](https://sonarcloud.io/summary/new_code?id=pm-2022-2-grupo-b_servico-aluguel)
