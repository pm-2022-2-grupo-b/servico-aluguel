package vadebici.model;

import vadebici.dom.*;
import vadebici.dom.dto.AluguelJsonApi;
import vadebici.dom.dto.CiclistaJsonApi;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class CiclistaModels {

    public static Ciclista getPostCiclista() {
        Ciclista ciclista = getCiclista();
        ciclista.setId(null);
        ciclista.setEmail("leandroabreusa@gmail.com");
        return ciclista;
    }

    public static Passaporte getPassaporte() {
        Passaporte passaporte = new Passaporte();
        passaporte.setNumero("CS265436");
        passaporte.setValidade(new GregorianCalendar(2024, Calendar.JANUARY,1, 12, 0, 0).getTime());
        passaporte.setPais("USA");
        return passaporte;
    }

    public static Ciclista getCiclista() {
        MeioDePagamento meioDePagamento = new MeioDePagamento();
        meioDePagamento.setNomeTitular("LEANDRO ABREU");
        meioDePagamento.setValidade(new GregorianCalendar(2024, Calendar.JANUARY,1, 12, 0, 0).getTime());
        meioDePagamento.setNumero("12345678910123");
        meioDePagamento.setCvv("123");

        Ciclista ciclista = new Ciclista();
        ciclista.setId("1");
        ciclista.setNome("Leandro");
        ciclista.setCpf("99999999999");
        ciclista.setEmail("leandroabreusa@edu.unirio.br");
        ciclista.setNacionalidade("BRA");
        ciclista.setSenha("12345678");
        ciclista.setStatus(true);
        ciclista.setNascimento(new GregorianCalendar(2000, Calendar.APRIL, 19, 12, 0, 0).getTime());
        ciclista.setMeioDePagamento(meioDePagamento);
        ciclista.setUrlFotoDocumento("https://imagens.ebc.com.br/JR7Fb8ISNmbrQZq0A-I6sSAbDDs=/1170x700/smart/https://agenciabrasil.ebc.com.br/sites/default/files/thumbnails/image/cpf_receita_federal0107200467.jpg?itok=iX_mVn8n");
        return ciclista;
    }

    public static CiclistaJsonApi putCiclistaJsonApi() {
        CiclistaJsonApi ciclistaJsonApi = new CiclistaJsonApi();
        ciclistaJsonApi.setId("1");
        ciclistaJsonApi.setNome("Leandro");
        ciclistaJsonApi.setCpf("22222222222");
        ciclistaJsonApi.setEmail("leandroabreusa@gmail.com");
        ciclistaJsonApi.setNacionalidade("BRA");
        ciclistaJsonApi.setStatus(true);
        ciclistaJsonApi.setNascimento(new GregorianCalendar(2000, Calendar.APRIL, 19, 12, 0, 0).getTime());
        ciclistaJsonApi.setUrlFotoDocumento("https://imagens.ebc.com.br/JR7Fb8ISNmbrQZq0A-I6sSAbDDs=/1170x700/smart/https://agenciabrasil.ebc.com.br/sites/default/files/thumbnails/image/cpf_receita_federal0107200467.jpg?itok=iX_mVn8n");
        return ciclistaJsonApi;
    }

    public static CiclistaJsonApi getCiclistaJsonApi() {
        CiclistaJsonApi ciclistaJsonApi = new CiclistaJsonApi();
        ciclistaJsonApi.setId("1");
        ciclistaJsonApi.setNome("Leandro");
        ciclistaJsonApi.setCpf("99999999999");
        ciclistaJsonApi.setEmail("leandroabreusa@edu.unirio.br");
        ciclistaJsonApi.setNacionalidade("BRA");
        ciclistaJsonApi.setStatus(true);
        ciclistaJsonApi.setNascimento(new Date());
        ciclistaJsonApi.setUrlFotoDocumento("https://imagens.ebc.com.br/JR7Fb8ISNmbrQZq0A-I6sSAbDDs=/1170x700/smart/https://agenciabrasil.ebc.com.br/sites/default/files/thumbnails/image/cpf_receita_federal0107200467.jpg?itok=iX_mVn8n");
        return ciclistaJsonApi;
    }


    public static Cobranca getCobranca() {
        Cobranca cobranca = new Cobranca();
        cobranca.setStatus("Aprovado");
        cobranca.setId("1234");
        cobranca.setValor(10.0);
        return cobranca;
    }

    public static AluguelJsonApi getAluguelJsonApi() {
        AluguelJsonApi aluguelJsonApi = new AluguelJsonApi();
        aluguelJsonApi.setCiclista("1");
        aluguelJsonApi.setTrancaInicio("123");
        return aluguelJsonApi;
    }

    public static Aluguel getAluguel() {
        Aluguel aluguel = new Aluguel();
        aluguel.setCiclista("1");
        aluguel.setBicicleta("123");
        aluguel.setTrancaInicio("123");
        aluguel.setHoraInicio(new Date());
        String[] cobrancas = new String[2];
        cobrancas[0] = getCobranca().getId();
        aluguel.setCobranca(cobrancas);
        return aluguel;
    }

    public static Tranca getTranca() {
        Tranca tranca = new Tranca();
        tranca.setStatus("OCUPADA");
        tranca.setId("123");
        tranca.setIdBicicleta("123");
        return tranca;
    }

    public static Bicicleta getBicicleta() {
        Bicicleta bicicleta = new Bicicleta();
        bicicleta.setStatus("DISPONIVEL");
        bicicleta.setId("123");
        return bicicleta;
    }

    public static Tranca getTrancaDisponivel() {
        Tranca tranca = new Tranca();
        tranca.setStatus("DISPONIVEL");
        tranca.setId("123");
        tranca.setIdBicicleta(null);
        return tranca;
    }

    public static Bicicleta getBicicletaOcupada() {
        Bicicleta bicicleta = new Bicicleta();
        bicicleta.setStatus("OCUPADA");
        bicicleta.setId("1");
        return bicicleta;
    }

}
