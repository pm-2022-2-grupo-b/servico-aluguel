package vadebici.model;

import vadebici.dom.Funcionario;

public class FuncionarioModels {
    public static Funcionario getFuncionario() {
        Funcionario funcionario = new Funcionario();
        funcionario.setNome("Leandro");
        funcionario.setId("1");
        funcionario.setIdade(22);
        funcionario.setCpf("99999999999");
        funcionario.setEmail("leandroabreusa@edu.unirio.br");
        funcionario.setSenha("senha");
        funcionario.setFuncao("Reparador");
        return funcionario;
    }

    public static Funcionario putFuncionario() {
        Funcionario funcionario = new Funcionario();
        funcionario.setNome("Leandro");
        funcionario.setIdade(22);
        funcionario.setCpf("22222222222");
        funcionario.setEmail("leandroabreusa@gmail.com");
        funcionario.setSenha("senha");
        funcionario.setFuncao("Reparador");
        return funcionario;
    }
}
