package vadebici.service;

import kong.unirest.HttpResponse;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import vadebici.db_in_memory.Database;
import vadebici.dom.Bicicleta;
import vadebici.dom.Tranca;
import vadebici.dom.dto.PostBicicletaJsonApi;
import vadebici.dom.dto.PostTrancaJsonApi;

import static org.junit.jupiter.api.Assertions.*;
import static vadebici.model.CiclistaModels.*;

class ServicoEquipamentoTest {

    private static final ServicoEquipamento SERVICO_EQUIPAMENTO = new ServicoEquipamento();

    private Bicicleta bicicleta;
    private Tranca tranca;


    @BeforeEach
    void setUp() {
        bicicleta = getBicicleta();
        bicicleta.setId("123");
        tranca = getTranca();
        tranca.setId("123");

        Database.carregarDados();
    }

    @AfterAll
    static void tearDown() {
        Database.limpar();
    }

    @Test
    void alteraStatusTranca_RetornaTrancaAlterada_QuandoBemSucedido() {
        HttpResponse<String> valida = SERVICO_EQUIPAMENTO.mudarStatusTranca(this.tranca.getId(), "OCUPADA");

        assertEquals(200, valida.getStatus());
    }

    @Test
    void alteraStatusBicicleta_RetornaBicicletaAlterada_QuandoBemSucedido() {
        HttpResponse<String> valida = SERVICO_EQUIPAMENTO.mudarStatusBicicleta(this.bicicleta.getId(), "DISPONIVEL");

        assertEquals(200, valida.getStatus());
    }

    @Test
    void insereBicicletaNaRede_RetornaTranca_QuandoBemSucedido() {
        PostTrancaJsonApi tranca = new PostTrancaJsonApi();
        tranca.setStatus("DISPONIVEL");
        tranca.setNumero(10);
        tranca.setAnoDeFabricacao("123");
        tranca.setLocalizacao("123");
        tranca.setModelo("123");

        String idTranca = SERVICO_EQUIPAMENTO.salvarTranca(tranca);
        SERVICO_EQUIPAMENTO.integrarTrancaNaRede(idTranca, "123");

        HttpResponse<String> valida = SERVICO_EQUIPAMENTO.integrarBicicletaNaRede(this.bicicleta.getId(), idTranca);

        assertEquals(200, valida.getStatus());
    }

    @Test
    void buscaBicicletaPorId_RetornaBicicleta_QuandoBemSucedido() {
        this.bicicleta = SERVICO_EQUIPAMENTO.buscarBicicleta(this.bicicleta.getId());

        assertNotNull(this.bicicleta);
    }

    @Test
    void buscaTrancaPorId_RetornaTranca_QuandoBemSucedido() {
        this.tranca = SERVICO_EQUIPAMENTO.buscarTranca(this.tranca.getId());

        assertNotNull(this.tranca);
    }

    @Test
    void removeBicicletaDaRede_RetornaTrancaSemBicicleta_QuandoBemSucedido() {
        PostBicicletaJsonApi bicicleta = new PostBicicletaJsonApi();

        bicicleta.setMarca("123");
        bicicleta.setModelo("123");
        bicicleta.setAno("123");
        bicicleta.setNumero(10);
        bicicleta.setStatus("DISPONIVEL");

        String idBicicleta = SERVICO_EQUIPAMENTO.salvarBicicleta(bicicleta);

        SERVICO_EQUIPAMENTO.integrarBicicletaNaRede(idBicicleta, "123");

        HttpResponse<String> valida = SERVICO_EQUIPAMENTO.removerBicicletaDaRede(idBicicleta, this.tranca.getId());

        assertEquals(200, valida.getStatus());
    }

}
