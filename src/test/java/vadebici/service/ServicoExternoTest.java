package vadebici.service;

import kong.unirest.HttpResponse;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import vadebici.db_in_memory.Database;
import vadebici.dom.*;
import vadebici.dom.dto.CobrancaJsonApi;

import static org.junit.jupiter.api.Assertions.*;
import static vadebici.model.CiclistaModels.*;

class ServicoExternoTest {
    private static final ServicoExterno SERVICO_EXTERNO = new ServicoExterno();

    private Ciclista ciclista;

    @BeforeEach
    void setUp() {
        ciclista = getCiclista();

        Database.carregarDados();
    }

    @AfterAll
    static void tearDown() {
        Database.limpar();
    }

    @Test
    void enviarEmail_RetornaTrue_QuandoBemSucedido() {
        Email email = new Email();
        email.setEnderecoEmail(ciclista.getEmail());
        email.setMensagem("E-mail de teste");
        HttpResponse<String> valida = SERVICO_EXTERNO.enviaEmail(email);

        assertEquals(200, valida.getStatus());
    }

    @Test
    void realizarCobranca_RetornaCobranca_QuandoBemSucedido() {
        CobrancaJsonApi cobrancaJsonApi = new CobrancaJsonApi();
        cobrancaJsonApi.setCiclista(ciclista.getId());
        cobrancaJsonApi.setValor(10.0);
        Cobranca cobranca = SERVICO_EXTERNO.realizaCobranca(cobrancaJsonApi);

        assertEquals(cobranca.getValor(), cobrancaJsonApi.getValor());
        assertNotNull(cobranca.getStatus());
        assertNotNull(cobranca.getId());
    }

    /*@Test
    void validarCartaoDoCiclista_RetornaTrue_QuandoBemSucedido() {
        HttpResponse<String> valida = SERVICO_EXTERNO.validaCartao(ciclista.getMeioDePagamento());

        assertEquals(200, valida.getStatus());
    }*/
}
