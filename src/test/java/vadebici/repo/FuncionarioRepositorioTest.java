package vadebici.repo;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import vadebici.db_in_memory.Database;
import vadebici.dom.Funcionario;
import vadebici.exception.GenericApiException;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static vadebici.model.FuncionarioModels.getFuncionario;

class FuncionarioRepositorioTest {

    private final FuncionarioRepositorio FUNC_REPO = new FuncionarioRepositorio(Database.TABELA_FUNCIONARIO);

    @BeforeEach
    void setUp() {
        Database.carregarDados();
    }

    @AfterAll
    static void setDown() {
        Database.limpar();
    }

    @Test
    void cadastrarFuncionario_RetornaFuncionario_QuandoBemSucedido() {
        Funcionario funcionario = getFuncionario();
        Funcionario respostaFuncionario = FUNC_REPO.guardaFuncionario(funcionario);

        assertEquals(funcionario, respostaFuncionario);
    }

    @Test
    void buscarFuncionarios_RetornaListaFuncionario_QuandoBemSucedido() {
        List<Funcionario> resultado = FUNC_REPO.retornaFuncionarios();
        assertEquals(1, resultado.size());
    }

    @Test
    void buscarFuncionario_LancaExcecao_QuandoNaoEncontrado() {
        GenericApiException ex = assertThrows(GenericApiException.class,
                () -> FUNC_REPO.retornaFuncionario("0"));
        assertEquals(404, ex.getCodigo());
        assertEquals("Funcionário não encontrado.", ex.getMensagem());
    }

    @Test
    void buscarFuncionario_RetornaFuncionario_QuandoBemSucedido() {
        Funcionario funcionario = getFuncionario();
        Funcionario resultado = FUNC_REPO.retornaFuncionario(funcionario.getId());
        assertEquals(funcionario, resultado);
    }

    @Test
    void deletarFuncionario_DeletarFuncionario_QuandoBemSucedido() {
        Funcionario funcionario = getFuncionario();
        String idFuncionario = funcionario.getId();
        FUNC_REPO.deletaFuncionario(idFuncionario);
        assertThrows(GenericApiException.class, () -> FUNC_REPO.retornaFuncionario(idFuncionario));
    }

    @Test
    void atualizarFuncionario_RetornaFuncionario_QuandoBemSucedido() {
        Funcionario funcionario = getFuncionario();
        funcionario.setNome("TROCADO");
        Funcionario respostaFuncionario = FUNC_REPO.alteraFuncionario(funcionario.getId(), funcionario);
        assertEquals(funcionario, respostaFuncionario);
    }

}