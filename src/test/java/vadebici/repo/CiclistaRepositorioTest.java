package vadebici.repo;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import vadebici.db_in_memory.Database;
import vadebici.dom.*;
import vadebici.dom.dto.CiclistaJsonApi;
import vadebici.exception.GenericApiException;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static vadebici.model.CiclistaModels.*;

class CiclistaRepositorioTest {
    @SuppressWarnings( "unchecked" )
    private static final Map<String, Aluguel> TABELA_ALUGUEL_MOCK = (HashMap<String, Aluguel>) mock(HashMap.class);
    @SuppressWarnings( "unchecked" )
    private static final Map<String, Ciclista> TABELA_CICLISTA_MOCK = (HashMap<String, Ciclista>) mock(HashMap.class);

    private static final CiclistaRepositorio CICLISTA_REPOSITORIO = new CiclistaRepositorio(TABELA_CICLISTA_MOCK, TABELA_ALUGUEL_MOCK);

    private Bicicleta bicicleta;
    private Tranca tranca;

    @BeforeEach
    void setUp() {
        bicicleta = getBicicleta();
        tranca = getTranca();

        when(TABELA_ALUGUEL_MOCK.put(any(), any())).thenReturn(null);
        when(TABELA_ALUGUEL_MOCK.replace(any(), any())).thenReturn(null);
        when(TABELA_CICLISTA_MOCK.put(any(), any())).thenReturn(null);
        when(TABELA_CICLISTA_MOCK.get(any())).thenReturn(getCiclista());
        when(TABELA_CICLISTA_MOCK.replace(any(), any())).thenReturn(null);

        Database.carregarDados();
    }

    @AfterAll
    static void tearDown() {
        Database.limpar();
    }

    @Test
    void existeEmail_RetornaVerdadeiro_QuandoExiste() {
        CiclistaRepositorio repoTemp = new CiclistaRepositorio(Database.TABELA_CICLISTA, null);

        Ciclista ciclista = getCiclista();

        boolean resultado = repoTemp.checaEmails(ciclista.getEmail());

        assertTrue(resultado);
    }

    @Test
    void existeEmail_RetornaFalso_QuandoNaoExiste() {
        boolean resultado = CICLISTA_REPOSITORIO.checaEmails("0");

        assertFalse(resultado);
    }

    @Test
    void cadastrarCiclista_RetornaCiclista_QuandoBemSucedido() {
        Ciclista ciclista = getPostCiclista();
        Ciclista respCiclista = CICLISTA_REPOSITORIO.cadastraCiclista(ciclista);

        assertNotNull(respCiclista.getId());
        assertEquals(ciclista.getEmail(), respCiclista.getEmail());
        assertEquals(ciclista.getCpf(), respCiclista.getCpf());
        assertEquals(ciclista.getMeioDePagamento(), respCiclista.getMeioDePagamento());
        assertEquals(ciclista.getNacionalidade(), respCiclista.getNacionalidade());
        assertEquals(ciclista.getNascimento(), respCiclista.getNascimento());
        assertEquals(ciclista.getSenha(), respCiclista.getSenha());
        assertEquals(ciclista.getPassaporte(), respCiclista.getPassaporte());
        assertEquals(ciclista.getStatus(), respCiclista.getStatus());
        assertEquals(ciclista.getUrlFotoDocumento(), respCiclista.getUrlFotoDocumento());

    }

    @Test
    void buscarCiclista_LancaExcecao_QuandoNaoEncontrado() {
        when(TABELA_CICLISTA_MOCK.get(any())).thenReturn(null);

        GenericApiException ex = assertThrows(GenericApiException.class,
                () -> CICLISTA_REPOSITORIO.retornaCiclista("0"));
        assertEquals(404, ex.getCodigo());
        assertEquals("Ciclista não encontrado", ex.getMensagem());
    }

    @Test
    void buscarCiclista_RetornaCiclista_QuandoBemSucedido() {
        Ciclista ciclista = getCiclista();
        Ciclista resultadoCiclista = CICLISTA_REPOSITORIO.retornaCiclista(ciclista.getId());
        assertEquals(ciclista, resultadoCiclista);
    }

    @Test
    void buscarCartaoPorCiclista_RetornaCartao_QuandoBemSucedido() {
        Ciclista ciclista = getCiclista();
        MeioDePagamento resultadoMeioDePagamento = CICLISTA_REPOSITORIO.retornaCartaoPorCiclista(ciclista.getId());
        assertEquals(ciclista.getMeioDePagamento(), resultadoMeioDePagamento);
    }

    @Test
    void confirmarEmail_MudaStatusParaConfirmado_QuandoBemSucedido() {
        CiclistaJsonApi ciclistaJsonApi = getCiclistaJsonApi();
        CICLISTA_REPOSITORIO.alteraStatusCiclista(ciclistaJsonApi.getId(), false);
        CICLISTA_REPOSITORIO.confirmaEmail(ciclistaJsonApi.getId());
        Ciclista respostaCiclista = CICLISTA_REPOSITORIO.retornaCiclista(ciclistaJsonApi.getId());

        assertTrue(respostaCiclista.getStatus());
    }

    @Test
    void confirmarEmail_LancaExcecao_QuandoJaConfirmado() {
        Ciclista ciclista = getCiclista();
        String idCiclista = ciclista.getId();

        GenericApiException ex = assertThrows(GenericApiException.class,
                () -> CICLISTA_REPOSITORIO.confirmaEmail(idCiclista));
        assertEquals(405, ex.getCodigo());
        assertEquals("Esta conta já foi confirmada.", ex.getMensagem());
    }

    @Test
    void alterarCiclista_RetornaCiclista_QuandoBemSucedido_Unitario() {
        String idCiclista = getCiclista().getId();
        CiclistaJsonApi ciclistaJsonApi = putCiclistaJsonApi();

        Ciclista ciclista = CICLISTA_REPOSITORIO.alteraCiclista(idCiclista, putCiclistaJsonApi());

        assertEquals(ciclistaJsonApi.getEmail(), ciclista.getEmail());
        assertEquals(ciclistaJsonApi.getNacionalidade(), ciclista.getNacionalidade());
        assertEquals(ciclistaJsonApi.getNascimento(), ciclista.getNascimento());
        assertEquals(ciclistaJsonApi.getCpf(), ciclista.getCpf());
        assertEquals(ciclistaJsonApi.getNome(), ciclista.getNome());
    }

    @Test
    void alterarCartaoPorCiclista_RetornaCartao_QuandoBemSucedido_Unitario() {
        Ciclista ciclista = getCiclista();

        MeioDePagamento meioDePagamento = ciclista.getMeioDePagamento();
        meioDePagamento.setNomeTitular("LEANDRO SA");

        CICLISTA_REPOSITORIO.alteraCartaoPorCiclista(ciclista.getId(), meioDePagamento);

        assertEquals(meioDePagamento.getCvv(), ciclista.getMeioDePagamento().getCvv());
        assertEquals(meioDePagamento.getValidade(), ciclista.getMeioDePagamento().getValidade());
        assertEquals(meioDePagamento.getNumero(), ciclista.getMeioDePagamento().getNumero());
        assertEquals(meioDePagamento.getNomeTitular(), ciclista.getMeioDePagamento().getNomeTitular());
    }

    @Test
    void alugarBicicleta_RetornaAluguel_QuandoBemSucedido_Unitario() {
        Aluguel aluguel = CICLISTA_REPOSITORIO.alugar(getCiclista(), getTranca(), getBicicleta(), getCobranca());

        assertEquals(getCiclista().getId(), aluguel.getCiclista());
        assertEquals(tranca.getId(), aluguel.getTrancaInicio());
        assertEquals(bicicleta.getId(), aluguel.getBicicleta());
        assertNotNull(aluguel.getHoraInicio());
    }


    @Test
    void devolverBicicleta_RetornaAluguelDevolvido_QuandoBemSucedido_Unitario() {
        Aluguel aluguel = CICLISTA_REPOSITORIO.devolver(getAluguel(), getTranca());

        assertEquals(getCiclista().getId(), aluguel.getCiclista());
        assertEquals(tranca.getId(), aluguel.getTrancaInicio());
        assertEquals(bicicleta.getId(), aluguel.getBicicleta());
        assertNotNull(aluguel.getHoraInicio());
    }

}