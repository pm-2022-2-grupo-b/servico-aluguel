package vadebici.controller;

import kong.unirest.HttpResponse;
import kong.unirest.JsonNode;
import kong.unirest.Unirest;
import kong.unirest.json.JSONObject;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import vadebici.db_in_memory.Database;
import vadebici.dom.*;
import vadebici.dom.dto.CiclistaJsonApi;
import vadebici.dom.dto.DevolucaoJsonApi;
import vadebici.repo.CiclistaRepositorio;
import vadebici.service.ServicoEquipamento;
import vadebici.service.ServicoExterno;
import vadebici.util.JavalinApp;
import vadebici.util.Validacao;

import java.text.SimpleDateFormat;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static vadebici.model.CiclistaModels.*;

@RunWith(MockitoJUnitRunner.class)
class CiclistaControllerTest {

    private static final JavalinApp app = new JavalinApp();
    private static final String BASE_URL = "http://localhost:7010";
    private Bicicleta bicicleta;
    private Tranca tranca;


    private static final ServicoExterno SERVICO_EXTERNO_MOCK = mock(ServicoExterno.class);

    private static final Validacao VALIDACAO_MOCK = mock(Validacao.class);

    private static final ServicoEquipamento SERVICO_EQUIPAMENTO_MOCK = mock(ServicoEquipamento.class);

    @SuppressWarnings( "unchecked" )
    private static final Map<String, Ciclista> TABELA_CICLISTA_MOCK = (HashMap<String, Ciclista>) mock(HashMap.class);

    @SuppressWarnings( "unchecked" )
    private static final Map<String, Aluguel> TABELA_ALUGUEL_MOCK = (HashMap<String, Aluguel>) mock(HashMap.class);


    private static final CiclistaRepositorio CICLISTA_REPOSITORIO = new CiclistaRepositorio(Database.TABELA_CICLISTA, Database.TABELA_ALUGUEL);

    @BeforeAll
    static void init() {
        app.start(7010);
    }
    @BeforeEach
    void setUp() {
        bicicleta = getBicicleta();
        tranca = getTranca();

        doNothing().when(VALIDACAO_MOCK).validaAtributos(isA(Object.class), isA(Integer.class));
        doNothing().when(VALIDACAO_MOCK).validaNacionalidadeJson(isA(CiclistaJsonApi.class));
        when(SERVICO_EXTERNO_MOCK.realizaCobranca(any())).thenReturn(getCobranca());
        when(TABELA_ALUGUEL_MOCK.put(any(), any())).thenReturn(null);
        when(TABELA_CICLISTA_MOCK.put(any(), any())).thenReturn(null);
        when(TABELA_CICLISTA_MOCK.replace(any(), any())).thenReturn(null);

        CiclistaController.validacao = VALIDACAO_MOCK;
        CiclistaController.servicoExterno = SERVICO_EXTERNO_MOCK;

        Database.carregarDados();
    }

    @AfterAll
    static void tearDown() {
        Database.limpar();
        app.stop();
    }

    @Test
    void existeEmail_RetornaVerdadeiro_QuandoExiste() {
        HttpResponse<String> response = Unirest.get(BASE_URL + "/ciclista/existeEmail/leandroabreusa@edu.unirio.br")
                .asString();

        assertEquals(200, response.getStatus());
        assertEquals("true", response.getBody());
    }

    @Test
    void existeEmail_RetornaFalso_QuandoNaoExiste() {
        HttpResponse<String> response = Unirest.get(BASE_URL + "/ciclista/existeEmail/xxx@gmail.com")
                .asString();

        assertEquals(200, response.getStatus());
        assertEquals("false", response.getBody());
    }

    @Test
    void cadastrarCiclista_RetornaCiclista_QuandoBemSucedido() {
        Ciclista ciclista = getPostCiclista();
        MeioDePagamento meioDePagamento = ciclista.getMeioDePagamento();
        HttpResponse<JsonNode> response = Unirest.post(BASE_URL + "/ciclista")
                .body(ciclista)
                .asJson();

        JSONObject jsonCiclista = response.getBody().getObject();
        JSONObject jsonMeioDePagamento = jsonCiclista.getJSONObject("meioDePagamento");

        assertEquals(201, response.getStatus());
        assertEquals(ciclista.getEmail(), jsonCiclista.get("email"));
        assertEquals(ciclista.getNacionalidade(), jsonCiclista.get("nacionalidade"));
        assertEquals(formatDate("yyyy-MM-dd", ciclista.getNascimento()), jsonCiclista.get("nascimento"));
        assertEquals(ciclista.getCpf(), jsonCiclista.get("cpf"));
        assertEquals(ciclista.getNome(), jsonCiclista.get("nome"));
        assertEquals(false, jsonCiclista.get("status"));
        assertEquals(ciclista.getSenha(), jsonCiclista.get("senha"));
        assertEquals(ciclista.getUrlFotoDocumento(), jsonCiclista.get("urlFotoDocumento"));

        assertEquals(meioDePagamento.getCvv(), jsonMeioDePagamento.get("cvv"));
        assertEquals(formatDate("yyyy-MM", meioDePagamento.getValidade()), jsonMeioDePagamento.get("validade"));
        assertEquals(meioDePagamento.getNumero(), jsonMeioDePagamento.get("numero"));
        assertEquals(meioDePagamento.getNomeTitular(), jsonMeioDePagamento.get("nomeTitular"));

    }

    @Test
    void cadastrarCiclistaEstrangeiro_RetornaCiclista_QuandoBemSucedido() {
        Ciclista ciclista = getPostCiclista();
        ciclista.setCpf(null);
        ciclista.setNacionalidade("USA");
        ciclista.setPassaporte(getPassaporte());
        MeioDePagamento meioDePagamento = ciclista.getMeioDePagamento();
        HttpResponse<JsonNode> response = Unirest.post(BASE_URL + "/ciclista")
                .body(ciclista)
                .asJson();

        JSONObject jsonCiclista = response.getBody().getObject();
        JSONObject jsonMeioDePagamento = jsonCiclista.getJSONObject("meioDePagamento");

        assertEquals(201, response.getStatus());
        assertEquals(ciclista.getEmail(), jsonCiclista.get("email"));
        assertEquals(ciclista.getNacionalidade(), jsonCiclista.get("nacionalidade"));
        assertEquals(formatDate("yyyy-MM-dd", ciclista.getNascimento()), jsonCiclista.get("nascimento"));
        assertEquals(ciclista.getCpf(), jsonCiclista.get("cpf"));
        assertEquals(ciclista.getNome(), jsonCiclista.get("nome"));
        assertEquals(false, jsonCiclista.get("status"));
        assertEquals(ciclista.getSenha(), jsonCiclista.get("senha"));
        assertEquals(ciclista.getUrlFotoDocumento(), jsonCiclista.get("urlFotoDocumento"));

        assertEquals(meioDePagamento.getCvv(), jsonMeioDePagamento.get("cvv"));
        assertEquals(formatDate("yyyy-MM", meioDePagamento.getValidade()), jsonMeioDePagamento.get("validade"));
        assertEquals(meioDePagamento.getNumero(), jsonMeioDePagamento.get("numero"));
        assertEquals(meioDePagamento.getNomeTitular(), jsonMeioDePagamento.get("nomeTitular"));

    }

    @Test
    void cadastrarCiclista_LancaExcecao_QuandoEstrangeiroSemPassaporte() {
        CiclistaController.validacao = new Validacao();

        Ciclista ciclista = getPostCiclista();
        ciclista.setId(null);
        ciclista.setNacionalidade("USA");
        ciclista.setCpf(null);

        HttpResponse<JsonNode> response = Unirest.post(BASE_URL + "/ciclista")
                .body(ciclista)
                .asJson();

        JSONObject jsonResponse = response.getBody().getObject();

        assertEquals(405, response.getStatus());
        assertEquals("O campo 'passaporte' é obrigatório para usuários estrangeiros", jsonResponse.get("mensagem"));
    }

    @Test
    void alterarCiclista_LancaExcecao_QuandoEstrangeiroSemPassaporte() {
        CiclistaController.validacao = new Validacao();

        CiclistaJsonApi ciclistaJsonApi = putCiclistaJsonApi();
        ciclistaJsonApi.setId(null);
        ciclistaJsonApi.setNacionalidade("USA");
        ciclistaJsonApi.setCpf(null);

        HttpResponse<JsonNode> response = Unirest.put(BASE_URL + "/ciclista/1")
                .body(ciclistaJsonApi)
                .asJson();

        JSONObject jsonResponse = response.getBody().getObject();

        assertEquals(405, response.getStatus());
        assertEquals("O campo 'passaporte' é obrigatório para usuários estrangeiros", jsonResponse.get("mensagem"));
    }

    @Test
    void alterarCiclista_LancaExcecao_QuandoBrasileiroSemCpf() {
        CiclistaController.validacao = new Validacao();

        CiclistaJsonApi ciclistaJsonApi = putCiclistaJsonApi();
        ciclistaJsonApi.setId(null);
        ciclistaJsonApi.setNacionalidade("BRA");
        ciclistaJsonApi.setCpf(null);

        HttpResponse<JsonNode> response = Unirest.put(BASE_URL + "/ciclista/1")
                .body(ciclistaJsonApi)
                .asJson();

        JSONObject jsonResponse = response.getBody().getObject();

        assertEquals(405, response.getStatus());
        assertEquals("O campo 'cpf' é obrigatório para usuários brasileiros", jsonResponse.get("mensagem"));
    }

    @Test
    void cadastrarCiclista_LancaExcecao_QuandoBrasileiroSemCpf() {
        CiclistaController.validacao = new Validacao();

        Ciclista ciclista = getPostCiclista();
        ciclista.setId(null);
        ciclista.setCpf(null);

        HttpResponse<JsonNode> response = Unirest.post(BASE_URL + "/ciclista")
                .body(ciclista)
                .asJson();

        JSONObject jsonResponse = response.getBody().getObject();

        assertEquals(405, response.getStatus());
        assertEquals("O campo 'cpf' é obrigatório para usuários brasileiros", jsonResponse.get("mensagem"));
    }

    @Test
    void cadastrarCiclista_LancaExcecao_QuandoEmailJaCadastrado() {
        Ciclista ciclista = getPostCiclista();
        ciclista.setEmail("leandroabreusa@edu.unirio.br");
        ciclista.setId(null);

        HttpResponse<JsonNode> response = Unirest.post(BASE_URL + "/ciclista")
                .body(ciclista)
                .asJson();

        JSONObject jsonResponse = response.getBody().getObject();

        assertEquals(405, response.getStatus());
        assertEquals("Email já cadastrado", jsonResponse.get("mensagem"));
    }

    @Test
    void buscarCiclista_LancaExcecao_QuandoNaoEncontrado() {
        HttpResponse<JsonNode> response = Unirest.get(BASE_URL + "/ciclista/0")
                .asJson();

        JSONObject jsonResponse = response.getBody().getObject();

        assertEquals(404, response.getStatus());
        assertEquals("Ciclista não encontrado", jsonResponse.get("mensagem"));
    }

    @Test
    void buscarCiclista_RetornaCiclista_QuandoBemSucedido() {
        Ciclista ciclista = getCiclista();
        MeioDePagamento meioDePagamento = ciclista.getMeioDePagamento();
        HttpResponse<JsonNode> response = Unirest.get(BASE_URL + "/ciclista/1")
                .asJson();

        JSONObject jsonCiclista = response.getBody().getObject();
        JSONObject jsonMeioDePagamento = jsonCiclista.getJSONObject("meioDePagamento");

        assertEquals(200, response.getStatus());
        assertEquals(ciclista.getEmail(), jsonCiclista.get("email"));
        assertEquals(ciclista.getNacionalidade(), jsonCiclista.get("nacionalidade"));
        assertEquals(formatDate("yyyy-MM-dd", ciclista.getNascimento()), jsonCiclista.get("nascimento"));
        assertEquals(ciclista.getCpf(), jsonCiclista.get("cpf"));
        assertEquals(ciclista.getNome(), jsonCiclista.get("nome"));
        assertEquals(ciclista.getStatus(), jsonCiclista.get("status"));
        assertEquals(ciclista.getSenha(), jsonCiclista.get("senha"));
        assertEquals(ciclista.getUrlFotoDocumento(), jsonCiclista.get("urlFotoDocumento"));

        assertEquals(meioDePagamento.getCvv(), jsonMeioDePagamento.get("cvv"));
        assertEquals(formatDate("yyyy-MM", meioDePagamento.getValidade()), jsonMeioDePagamento.get("validade"));
        assertEquals(meioDePagamento.getNumero(), jsonMeioDePagamento.get("numero"));
        assertEquals(meioDePagamento.getNomeTitular(), jsonMeioDePagamento.get("nomeTitular"));
    }

    @Test
    void buscarCartaoPorCiclista_RetornaCartao_QuandoBemSucedido() {
        MeioDePagamento meioDePagamento = getCiclista().getMeioDePagamento();
        HttpResponse<JsonNode> response = Unirest.get(BASE_URL + "/cartaoDeCredito/1")
                .asJson();

        JSONObject jsonResponse = response.getBody().getObject();

        assertEquals(200, response.getStatus());
        assertEquals(meioDePagamento.getCvv(), jsonResponse.get("cvv"));
        assertEquals(formatDate("yyyy-MM", meioDePagamento.getValidade()), jsonResponse.get("validade"));
        assertEquals(meioDePagamento.getNumero(), jsonResponse.get("numero"));
        assertEquals(meioDePagamento.getNomeTitular(), jsonResponse.get("nomeTitular"));
    }

    @Test
    void confirmarEmail_MudaStatusParaConfirmado_QuandoBemSucedido() {
        CiclistaJsonApi ciclistaJsonApi = getCiclistaJsonApi();
        CICLISTA_REPOSITORIO.alteraStatusCiclista(ciclistaJsonApi.getId(), false);
        HttpResponse<JsonNode> response = Unirest.post(BASE_URL + "/ciclista/1/ativar")
                .asJson();

        assertEquals(200, response.getStatus());
    }

    @Test
    void confirmarEmail_LancaExcecao_QuandoJaConfirmado() {
        HttpResponse<JsonNode> response = Unirest.post(BASE_URL + "/ciclista/1/ativar")
                .asJson();

        assertEquals(405, response.getStatus());
        assertEquals("Esta conta já foi confirmada.", response.getBody().getObject().get("mensagem"));
    }

    @NotNull
    private String formatDate(String pattern, Date ciclista) {
        return new SimpleDateFormat(pattern).format(ciclista);
    }

    private void enviaEmail(String destinatario, String mensagem) {
        Email email = new Email();
        email.setEnderecoEmail(destinatario);
        email.setMensagem(mensagem);
        SERVICO_EXTERNO_MOCK.enviaEmail(email);
    }


//    @Test
//    void alugarBicicleta_RetornaAluguel_QuandoBemSucedido_Integracao() {
//        AluguelJsonApi aluguelJsonApi = getAluguelJsonApi();
//        aluguelJsonApi.setTrancaInicio(this.tranca.getId());
//        HttpResponse<String> response = Unirest.post(BASE_URL + "/aluguel")
//                .body(aluguelJsonApi)
//                .asString();
//
//        // Pega o ID aleatório gerado no aluguel e substitui pelo ID padrão da bicicleta
//        Aluguel jsonResponse = JavalinJson.fromJson(response.getBody(), Aluguel.class);
//
//        // Isso foi feito, pois como o sistema não está integrado ainda,
//        // ele gera um ID fictícioa aleatório de bicicleta para permitir a realização do aluguel
//
//        assertEquals(200, response.getStatus());
//        assertEquals(aluguelJsonApi.getCiclista(), jsonResponse.getCiclista());
//        assertEquals(aluguelJsonApi.getTrancaInicio(), jsonResponse.getTrancaInicio());
//        assertEquals(bicicleta.getId(), jsonResponse.getBicicleta());
//        assertNotNull(jsonResponse.getCobranca());
//        assertNotNull(jsonResponse.getHoraInicio());
//    }

//    @Test
//    void alugarBicicleta_LancaGenericException_QuandoCiclistaJaPossuiEmprestimo_Integracao() {
//        AluguelJsonApi aluguelJsonApi = getAluguelJsonApi();
//        aluguelJsonApi.setTrancaInicio(this.tranca.getId());
//        HttpResponse<String> response = Unirest.post(BASE_URL + "/aluguel")
//                .body(aluguelJsonApi)
//                .asString();
//
//        // Pega o ID aleatório gerado no aluguel e substitui pelo ID padrão da bicicleta
//        Aluguel jsonResponse = JavalinJson.fromJson(response.getBody(), Aluguel.class);
//
//        // Isso foi feito, pois como o sistema não está integrado ainda,
//        // ele gera um ID fictícioa aleatório de bicicleta para permitir a realização do aluguel
//
//        AluguelJsonApi novoAluguel = getAluguelJsonApi();
//        novoAluguel.setTrancaInicio(this.tranca.getId());
//        HttpResponse<JsonNode> responseNovoAluguel = Unirest.post(BASE_URL + "/aluguel")
//                .body(aluguelJsonApi)
//                .asJson();
//
//        assertEquals(422, responseNovoAluguel.getStatus());
//    }

    @Test
    void alterarCiclista_RetornaCiclista_QuandoBemSucedido_Integracao() {
        CiclistaJsonApi ciclistaJsonApi = putCiclistaJsonApi();
        HttpResponse<JsonNode> response = Unirest.put(BASE_URL + "/ciclista/1")
                .body(ciclistaJsonApi)
                .asJson();

        JSONObject jsonResponse = response.getBody().getObject();

        assertEquals(200, response.getStatus());
        assertEquals(ciclistaJsonApi.getEmail(), jsonResponse.get("email"));
        assertEquals(ciclistaJsonApi.getNacionalidade(), jsonResponse.get("nacionalidade"));
        assertEquals(formatDate("yyyy-MM-dd", ciclistaJsonApi.getNascimento()), jsonResponse.get("nascimento"));
        assertEquals(ciclistaJsonApi.getCpf(), jsonResponse.get("cpf"));
        assertEquals(ciclistaJsonApi.getNome(), jsonResponse.get("nome"));
        assertEquals(ciclistaJsonApi.getStatus(), jsonResponse.get("status"));
    }

    @Test
    void alterarCiclista_LancaException_QuandoEmailJaCadastrado_Integracao() {
        CiclistaJsonApi ciclistaJsonApi = putCiclistaJsonApi();
        ciclistaJsonApi.setEmail("leandroabreusa@edu.unirio.br");
        HttpResponse<JsonNode> response = Unirest.put(BASE_URL + "/ciclista/1")
                .body(ciclistaJsonApi)
                .asJson();

        JSONObject jsonResponse = response.getBody().getObject();

        assertEquals(405, response.getStatus());
        assertEquals("Email já cadastrado", jsonResponse.get("mensagem"));
    }

    @Test
    void alterarCartaoPorCiclista_RetornaCartao_QuandoBemSucedido_Integracao() {
        MeioDePagamento meioDePagamento = getCiclista().getMeioDePagamento();
        meioDePagamento.setNomeTitular("LEANDRO SA");
        HttpResponse<JsonNode> response = Unirest.put(BASE_URL + "/cartaoDeCredito/1")
                .body(meioDePagamento)
                .asJson();

        assertEquals(200, response.getStatus());

    }

    @Test
    void permiteAluguelPorCiclista_RetornaTrue_QuandoBemSucedido_Integracao() {
        String ciclistaId = getCiclista().getId();
        HttpResponse<String> response = Unirest.get(BASE_URL + "/ciclista/" + ciclistaId + "/permiteAluguel")
                .asString();

        String jsonResponse = response.getBody();

        assertEquals(200, response.getStatus());
        assertTrue(Boolean.parseBoolean(jsonResponse));
    }

    @Test
    void notificaAluguelEmCursoPorCiclista_RetornaCiclista_QuandoBemSucedido_Integracao() {
        Ciclista ciclista = CICLISTA_REPOSITORIO.retornaCiclista(getCiclista().getId());
        Cobranca cobranca = getCobranca();
        CICLISTA_REPOSITORIO.alugar(ciclista, tranca, bicicleta, cobranca);
        HttpResponse<JsonNode> response = Unirest.post(BASE_URL + "/ciclista/" + ciclista.getId() + "/notificaAluguelEmCurso")
                .asJson();

        JSONObject jsonResponse = response.getBody().getObject();

        assertEquals(200, response.getStatus());
        assertEquals(ciclista.getEmail(), jsonResponse.get("email"));
        assertEquals(ciclista.getNacionalidade(), jsonResponse.get("nacionalidade"));
        assertEquals(formatDate("yyyy-MM-dd", ciclista.getNascimento()), jsonResponse.get("nascimento"));
        assertEquals(ciclista.getCpf(), jsonResponse.get("cpf"));
        assertEquals(ciclista.getNome(), jsonResponse.get("nome"));

    }

//    @Test
//    void devolverBicicleta_RetornaAluguel_QuandoBemSucedido() {
//        AluguelJsonApi aluguelJsonApi = getAluguelJsonApi();
//        HttpResponse<String> response = Unirest.post(BASE_URL + "/aluguel")
//                .body(aluguelJsonApi)
//                .asString();
//
//        // Pega o ID aleatório gerado no aluguel e substitui pelo ID padrão da bicicleta
//        Aluguel alugadoJson = JavalinJson.fromJson(response.getBody(), Aluguel.class);
//
//        // Isso foi feito, pois como o sistema não está integrado ainda,
//        // ele gera um ID fictício aleatório de bicicleta para permitir a realização do aluguel
//
//        DevolucaoJsonApi devolucaoJsonApi = new DevolucaoJsonApi();
//        devolucaoJsonApi.setIdBicicleta(bicicleta.getId());
//        devolucaoJsonApi.setIdTranca(UUID.randomUUID().toString());
//
//        HttpResponse<JsonNode> devolvido = Unirest.post(BASE_URL + "/devolucao")
//                .body(devolucaoJsonApi)
//                .asJson();
//
//        JSONObject devolvidoJson = devolvido.getBody().getObject();
//
//        assertEquals(200, devolvido.getStatus());
//        assertEquals(alugadoJson.getCiclista(), devolvidoJson.get("ciclista"));
//        assertEquals(alugadoJson.getTrancaInicio(), devolvidoJson.get("trancaInicio"));
//        assertEquals(alugadoJson.getTrancaFim(), devolvidoJson.get("bicicleta"));
//        assertNotNull(devolvidoJson.get("cobranca"));
//        assertNotNull(devolvidoJson.get("trancaFim"));
//        assertNotNull(devolvidoJson.get("horaInicio"));
//        assertNotNull(devolvidoJson.get("horaFim"));
//    }
//
    @Test
    void devolverBicicleta_LancaGenericException_QuandoBicicletaNaoAlugada_Integracao() {
        DevolucaoJsonApi devolucaoJsonApi = new DevolucaoJsonApi();
        devolucaoJsonApi.setIdBicicleta(bicicleta.getId());
        devolucaoJsonApi.setIdTranca("123");

        HttpResponse<JsonNode> devolvido = Unirest.post(BASE_URL + "/devolucao")
                .body(devolucaoJsonApi)
                .asJson();

        assertEquals(422, devolvido.getStatus());
    }

}
