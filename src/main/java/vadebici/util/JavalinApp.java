package vadebici.util;

import io.javalin.Javalin;
import io.javalin.plugin.json.JavalinJson;
import vadebici.controller.FuncionarioController;
import vadebici.controller.CiclistaController;
import vadebici.exception.GenericApiException;

import static io.javalin.apibuilder.ApiBuilder.*;

public class JavalinApp {

    private final Javalin app =
            Javalin.create(config -> config.defaultContentType = "application/json")
                    .routes(() -> {
                        final String FUNCIONARIO_COM_ID = "/funcionario/:idFuncionario";

                        path("/funcionario", () -> get(FuncionarioController::retornaFuncs));
                        path("/funcionario", () -> post(FuncionarioController::guardaFunc));

                        path(FUNCIONARIO_COM_ID, () -> get(FuncionarioController::retornaFunc));
                        path(FUNCIONARIO_COM_ID, () -> delete(FuncionarioController::deletaFunc));
                        path(FUNCIONARIO_COM_ID, () -> put(FuncionarioController::alteraFunc));

                        path("/ciclista", () -> post(CiclistaController::guardaCiclista));
                        path("/ciclista/:idCiclista", () -> get(CiclistaController::retornaCiclista));
                        path("/ciclista/:idCiclista", () -> put(CiclistaController::alteraCiclista));
                        path("/ciclista/:idCiclista/ativar", () -> post(CiclistaController::ativaCadastro));
                        path("/ciclista/existeEmail/", () -> get(CiclistaController::emailNaoEnviado));
                        path("/ciclista/existeEmail/:email", () -> get(CiclistaController::verificaEmail));
                        path("/ciclista/:idCiclista/permiteAluguel", () -> get(CiclistaController::permiteAluguel));
                        path("/ciclista/:idCiclista/notificaAluguelEmCurso", () -> post(CiclistaController::notificaAluguelEmCurso));

                        path("/cartaoDeCredito/:idCiclista", () -> get(CiclistaController::retornaCartao));
                        path("/cartaoDeCredito/:idCiclista", () -> put(CiclistaController::alteraCartao));

                        path("/aluguel", () -> post(CiclistaController::alugar));
                        path("/devolucao", () -> post(CiclistaController::devolver));

                    }).error(500, ctx -> {
                        GenericApiException ex = new GenericApiException(500, "Ocorreu um erro durante a operação");
                        ctx.result(JavalinJson.toJson(ex));
                    }).error(404, ctx -> {
                        if (ctx.attribute("handle") == null) {
                            GenericApiException ex = new GenericApiException(404, "Página não encontrada");
                            ctx.result(JavalinJson.toJson(ex));
                        }
                    })
                    .exception(GenericApiException.class, (e, ctx) -> {
                        ctx.attribute("handle", true);
                        String response = JavalinJson.getToJsonMapper().map(e);
                        ctx.status(e.getCodigo());
                        ctx.result(response);
                    });


    public Javalin start(int port) {
        return this.app.start(port);
    }

    public void stop()  {
        this.app.stop();
    }
}
